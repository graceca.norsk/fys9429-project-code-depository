---
title: "Cancer_dataset_machine_learning_KNN_SVM"
author: "Wanxin Lai"
date: "2023-04-02"
output: html_document
---

```{r message=FALSE}
library(e1071)
#library(libsvm)
library(class)
```


###Data

Data loading:
```{r}
load(file = "TCGA.RData")

cat("Raw: Genes & Samples =", dim(expr), "\n")

table(classes)

```
Get three objects:

expr: A read count table

classes: A vector with class labels for the columns in expr. The classes are Breast, Kidney, Colon, Lung, and Prostate

samples: A vector with a more detailed classification than what is in classes.

#### Data normalisation
```{r}
#Normalisation of data
library(DESeq2)

#get rid of decimal numbers, software expect only integers
counttable <- data.frame(round(expr))

# Create an object followed by VST transformation
coldata <- data.frame(colnames(counttable), classes,row.names=1)
dds <- DESeqDataSetFromMatrix(counttable, coldata, ~classes)

# calculating the gene-wise dispersion estimates, to fit a trend line through the dispersions over the mean. 
# Only the trend line is then used by the transformations, not the gene-wise estimates. 
# For visualisation, clustering, or machine learning applications is recommend with blind=FALSE.
expr.norm <- assay(varianceStabilizingTransformation(dds, blind = FALSE))

```

####PCA
```{r}
expr.pcat <- prcomp(t(expr.norm)) #t() because it operates on row, so t() to samples, points is also the samples 
#summary(expr.pcat)

plot(expr.pcat$x[,1],expr.pcat$x[,2], xlab = "PC1", ylab = "PC2", pch =16, col=classes)

legend("topleft", legend = levels(as.factor(classes)),pch = 16, col=1:5, cex=0.7)
```

Based on the PCA plot, these samples are classified well using machine learning because the PCA plot shows that by using only two principal components(the largest and second largest total variation). The data set is well separated into 5 different clusters with only two dimensions.

```{r}

plot(expr.pcat$x[,2],expr.pcat$x[,3], xlab = "PC2", ylab = "PC3", pch =16, col=classes)

legend("topleft", legend = levels(as.factor(classes)),pch = 16, col=1:5, cex=0.7)
```


```{r}
var.expl <- expr.pcat$sdev^2 / sum(expr.pcat$sdev^2)*100

paste0("Variance explained: ", paste0(format(var.expl[1:10], digits = 2), collapse = " "),", I will include the first 10 PCs to explains more than 95% of variance.", collapse = " ")
```


```{r}

# set a cut off to filter important genes for loadings
loading_cutoff <- sqrt(1/ncol(t(expr.norm)))



# we look at first 9 pcs here:
for(pcs_select in 1:5) {
  # Create an empty list for those that pass the cutoff
  passing_rownames <- vector()
  
  # setting keep_row = FALSE
  for(pc_row in 1:nrow(expr.pcat$rotation)) {
    keep_row <- FALSE
    
    # for the first n PCs selected
    for (pc_col in 1:pcs_select) {
      if (abs(expr.pcat$rotation[pc_row, pc_col]) > loading_cutoff) {
        keep_row <- TRUE
      }
    }
    
    if (keep_row == TRUE) {
      passing_rownames <- c(passing_rownames, rownames(expr.pcat$rotation)[pc_row])
    }
  }
  
  # now we know which genes have the most influence on each selected PC axis.
  cat(paste(length(passing_rownames), " features/genes pass the loading cutoff of ", round(loading_cutoff, 4), " when the first ", pcs_select, " pc(s) are selected \n", sep = "" ))
}
```

```{r}
pcs_select <- 2

passing_rownames <- vector()
  
  # setting keep_row = FALSE
  for(pc_row in 1:nrow(expr.pcat$rotation)) {
    keep_row <- FALSE
    
    # for the first n PCs selected
    for (pc_col in 1:pcs_select) {
      if (abs(expr.pcat$rotation[pc_row, pc_col]) > loading_cutoff) {
        keep_row <- TRUE
      }
    }
    
    if (keep_row == TRUE) {
      passing_rownames <- c(passing_rownames, rownames(expr.pcat$rotation)[pc_row])
    }
  }

top_loadings <- expr.pcat$rotation[passing_rownames, ][,1:pcs_select]

new.data <- expr.pcat$x[,1:pcs_select] %*% t(top_loadings)

#new data with reduced dimensions
reduced.genes.data<- t(new.data)

colnames(reduced.genes.data) <- rownames(expr.pcat$x)
#head(reduced.genes.data)

write.table(reduced.genes.data, file = paste("./Cancer_Feature_Select_", pcs_select, "pcs_", length(passing_rownames), "mRNA.txt", sep = ""),sep = "\t", quote = FALSE)
```

```{r new reduced genes data}
cat(paste("After feature selection, the new data has ", nrow(reduced.genes.data), " rows (genes)", " and ", ncol(reduced.genes.data), " columns (samples).", sep = "")) 

```

### k-NN on the cancer RNA-seq data
```{r}
#split data into 50/50
n <- nrow(t(expr.norm))

# Randomize row numbers for two sets of samples using sample()
# then split the data into two sets, either into train or test, using split(), creating a list of selected row numbers
selected.rows <- split(sample(n), rep(c("train", "test"), length = n))

# Extract the sample labels
cancer.labels <- coldata$classes

# Transform and copy the expr data 
# note that it does not have labels for cancer types
cancer.unlabelled <- t(expr.norm)

# Construct the training and test datasets using selected row numbers from one of the split sets
cancer.train <- cancer.unlabelled[selected.rows$train, ]
cancer.test <- cancer.unlabelled[selected.rows$test, ]

```

```{r}
set.seed(1)

# To the `knn` function, we pass the training data, the test data, and the sample species labels for the training data.

cancer.knn <- knn(cancer.train, cancer.test, cancer.labels[selected.rows$train], k = 4)

table(cancer.labels[selected.rows$test], cancer.knn, dnn = c("Real", "Predicted"))
```

```{r}
# try with different k value 
cancer.knn.10 <- knn(cancer.train, cancer.test, cancer.labels[selected.rows$train], k = 10)

table(cancer.labels[selected.rows$test], cancer.knn, dnn = c("Real", "Predicted"))
```

```{r}
# save the confusion table 
confusion <- table(cancer.labels[selected.rows$test], cancer.knn, dnn = c("Real", "Predicted"))

confusion.10 <- table(cancer.labels[selected.rows$test], cancer.knn.10, dnn = c("Real", "Predicted"))
```

```{r}
# Calculate Accuracy, use the `diag` function to get the diagonal elements in the matrix. By summing them, we get the number of correct predictions, and dividing them with the total number of samples, we get the accuracy as the fraction of correct predictions

accuracy <- sum(diag(confusion)) / sum(confusion)
error <- 1 - accuracy

accuracy.10 <- sum(diag(confusion.10)) / sum(confusion.10)
error <- 1 - accuracy.10
```
WE want to pick the best numbers of neighbours, best value of k.
```{r}
#set range of k , k is the numbers of premium neighbours
ks <- c(2, 4, 6, 8, 10, 12, 14, 16, 18, 20)   


#randomly subset our data in k folds of roughly equal size.
k_folds <- 20  
N <- nrow(cancer.unlabelled)

ind_train <- factor(sample(x = rep(1:k_folds, 
                                   each = N / k_folds),  # Sample IDs for training data
                           size = N))
# table below shows 50 observation for every fold.
table(ind_train)                        

nrun <- 100

pred_accuracy <- c()
for (k in ks) {
  preds <- c()
  for (group in 1:k_folds) {
    for (i in seq_len(nrun)) {
      k.pred <- knn.cv(cancer.unlabelled[ind_train == group,], cancer.labels[ind_train == group], k = k)
      confusion <- table(k.pred, cancer.labels[ind_train == group])
      accuracy = sum(diag(confusion)) / sum(confusion)
      preds <- c(preds, accuracy)
    }
  }
  pred_accuracy <- c(pred_accuracy, mean(preds))
}
```

```{r}
plot(ks, pred_accuracy, type = "o", pch = 16, col = 1, ylab = "Mean accuracy", xlab = "k")


```

```{r}
pred_accuracy
```

#test with reduced features
```{r}
cancer.labels <- coldata$classes

# Transform and copy the expr data 
# note that it does not have labels for cancer types
cancer.unlabelled.reduced <- t(reduced.genes.data)
meta_idx <- match(rownames(coldata), rownames(cancer.unlabelled.reduced))

#set range of k , k is the numbers of premium neighbours
ks <- c(2, 4, 6, 8, 10, 12, 14, 16, 20)   


#randomly subset our data in k folds of roughly equal size.
k_folds <- 10  
N <- nrow(cancer.unlabelled.reduced)

ind_train <- factor(sample(x = rep(1:k_folds, 
                                   each = N / k_folds),  # Sample IDs for training data
                           size = N))
# table below shows 50 observation for every fold.
table(ind_train)                        

nrun <- 100

pred_accuracy <- c()
for (k in ks) {
  preds <- c()
  for (group in 1:k_folds) {
    for (i in seq_len(nrun)) {
      k.pred <- knn.cv(cancer.unlabelled.reduced[ind_train == group,], cancer.labels[ind_train == group], k = k)
      confusion <- table(k.pred, cancer.labels[ind_train == group])
      accuracy = sum(diag(confusion)) / sum(confusion)
      preds <- c(preds, accuracy)
    }
  }
  pred_accuracy <- c(pred_accuracy, mean(preds))
}
```

```{r}
plot(ks, pred_accuracy, type = "o", pch = 16, col = 1, ylab = "Mean accuracy", xlab = "k")

```

```{r}
# Construct the training and test datasets using selected row numbers from one of the split sets
cancer.train <- cancer.unlabelled.reduced[selected.rows$train, ]
cancer.test <- cancer.unlabelled.reduced[selected.rows$test, ]

cancer.knn.10 <- knn(cancer.train, cancer.test, cancer.labels[selected.rows$train], k = 10)

table(cancer.labels[selected.rows$test], cancer.knn, dnn = c("Real", "Predicted"))
```


#### Support Vector Machine  

##### Using default settings: Radial Basis function (kernel)

```{r}
# Load the library with svm function
library(e1071)

# Train the model using the training data
 
# Start by getting the indices for all possible test sets
nsets <- 10 # The number of sets of different training/test data we want to use for cross validation
expr.normt <- t(expr.norm)

# The number of sets of different training/test data we want to use for cross validation
n <- nrow(expr.normt) 
cv.sets <- split(sample(n), rep(seq_len(nsets), length = n)) # Create 10 different random sets of sample row numbers

accuracies <- NULL

# Iterate through the different sets of row numbers...
for (set in cv.sets) {
  # The current row numbers (`set`) corresponds to the test data, so
  # for training, we use `-set` to use all samples but the test samples.
  svm.model <- svm(expr.normt[-set, ], classes[-set])
  svm.pred <- predict(svm.model, expr.normt[set, ])
  confusion <- table(classes[set], svm.pred)
  # Calculate current accuracy, and add to the list of accuracies
  accuracies <- c(accuracies, sum(diag(confusion)) / sum(confusion))
}
# Calculate the mean accuracy from the 10 tests
mean_accuracy <- mean(accuracies)
mean_accuracy
```

```{r grid search on RBF full features}
print(date())

C <- c(0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000)
gamma <- c(0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000)

acc.df <- cbind(expand.grid(C = C, gamma = gamma), accuracy = NA)

for (i in seq_len(nrow(acc.df))) {
  svm.model <- svm(expr.normt[-set, ], classes[-set], cost = acc.df[i, "C"], gamma = acc.df[i, "gamma"])
  svm.pred <- predict(svm.model, expr.normt[set, ])
  confusion <- table(classes[set], svm.pred)
  acc.df$accuracy[i] <- sum(diag(confusion)) / sum(confusion)
}
print(date())
```

```{r}
# Plot contours of accuracies for different C and gamma values
library(ggplot2)
library(directlabels)
accuracy.contour <- ggplot(acc.df, aes(C, gamma, z = accuracy)) +
  stat_contour(aes(colour = ..level..), binwidth = 0.15) +
  scale_colour_gradient(low = "tomato", high = "forestgreen") +
  scale_x_log10("C") + scale_y_log10(bquote(gamma)) + theme_bw()
direct.label(accuracy.contour, "top.pieces")

```

```{r}
C <- 100
G <- 0.00001

print(date())
for (set in cv.sets) {
  # The current row numbers (`set`) corresponds to the test data, so
  # for training, we use `-set` to use all samples but the test samples.
  svm.model <- svm(expr.normt[-set, ], classes[-set], cost = C, gamma = G )
  svm.pred <- predict(svm.model, expr.normt[set, ])
  confusion <- table(classes[set], svm.pred)
  # Calculate current accuracy, and add to the list of accuracies
  accuracies <- c(accuracies, sum(diag(confusion)) / sum(confusion)) 
}

print(date())
# Calculate the mean accuracy from the 10 tests
mean_accuracy <- mean(accuracies)
mean_accuracy
```


```{r grid search on RBF reduced features}
print(date())

C <- c(0.001, 0.01, 0.1, 1, 10, 100, 1000, 10000)
gamma <- c(0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10, 100, 1000)

acc.df <- cbind(expand.grid(C = C, gamma = gamma), accuracy = NA)

for (i in seq_len(nrow(acc.df))) {
  svm.model <- svm(cancer.unlabelled.reduced[selected.rows$train, ], coldata$classes[selected.rows$train], cost = acc.df[i, "C"], gamma = acc.df[i, "gamma"])
  svm.pred.reduced <- predict(svm.model, cancer.unlabelled.reduced[selected.rows$test, ])
  confusion.reduced <- table(coldata$classes[selected.rows$test], svm.pred.reduced)
  acc.df$accuracy[i] <- sum(diag(confusion.reduced)) / sum(confusion.reduced)
}
print(date())
```

```{r}
# Plot contours of accuracies for different C and gamma values
library(ggplot2)
library(directlabels)
accuracy.contour <- ggplot(acc.df, aes(C, gamma, z = accuracy)) +
  stat_contour(aes(colour = ..level..), binwidth = 0.15) +
  scale_colour_gradient(low = "tomato", high = "forestgreen") +
  scale_x_log10("C") + scale_y_log10(bquote(gamma)) + theme_bw()
direct.label(accuracy.contour, "top.pieces")


```


#### test with reduced features
```{r}
C <- 1000
G <- 0.00001

nsets <- 10
# The number of sets of different training/test data we want to use for cross validation
n <- nrow(cancer.unlabelled.reduced) 
cv.sets <- split(sample(n), rep(seq_len(nsets), length = n)) # Create 10 different random sets of sample row numbers

accuracies <- NULL


cancer.train <- cancer.unlabelled.reduced[selected.rows$train, ]
cancer.test <- cancer.unlabelled.reduced[selected.rows$test, ]

classes.reduced.train <- coldata$classes[selected.rows$train]
classes.reduced.test <- coldata$classes[selected.rows$test]

print(date())
for (set in cv.sets) {
  # The current row numbers (`set`) corresponds to the test data, so
  # for training, we use `-set` to use all samples but the test samples.
  svm.model <- svm(cancer.train, classes.reduced.train, cost = C, gamma = G )
  svm.pred <- predict(svm.model, cancer.test)
  confusion <- table(classes.reduced.test, svm.pred)
  # Calculate current accuracy, and add to the list of accuracies
  accuracies <- c(accuracies, sum(diag(confusion)) / sum(confusion)) 
}

print(date())
# Calculate the mean accuracy from the 10 tests
mean_accuracy <- mean(accuracies)
mean_accuracy
```

##### Using Polynomials

```{r}
accuracies <- NULL

print(date())
for (set in cv.sets) {
  # The current row numbers (`set`) corresponds to the test data, so
  # for training, we use `-set` to use all samples but the test samples.
  svm.model <- svm(expr.normt[-set, ], classes[-set], kernel = "polynomial", cost = C, gamma = G)
  svm.pred <- predict(svm.model, expr.normt[set, ])
  confusion <- table(classes[set], svm.pred)
  # Calculate current accuracy, and add to the list of accuracies
  accuracies <- c(accuracies, sum(diag(confusion)) / sum(confusion)) 
}

print(date())
# Calculate the mean accuracy from the 10 tests
mean_accuracy <- mean(accuracies)
mean_accuracy
```

```{r}
accuracies <- NULL

for (set in cv.sets) {
  # The current row numbers (`set`) corresponds to the test data, so
  # for training, we use `-set` to use all samples but the test samples.
  svm.model <- svm(cancer.train, classes.reduced.train, kernel = "polynomial",cost = C, gamma = G)
  svm.pred <- predict(svm.model, cancer.test)
  confusion <- table(classes.reduced.test, svm.pred)
  # Calculate current accuracy, and add to the list of accuracies
  accuracies <- c(accuracies, sum(diag(confusion)) / sum(confusion)) 
}

mean_accuracy <- mean(accuracies)
mean_accuracy
```


##### Using linear
```{r}
accuracies <- NULL

print(date())
for (set in cv.sets) {
  # The current row numbers (`set`) corresponds to the test data, so
  # for training, we use `-set` to use all samples but the test samples.
  svm.model <- svm(expr.normt[-set, ], classes[-set], kernel = "linear")
  svm.pred <- predict(svm.model, expr.normt[set, ])
  confusion <- table(classes[set], svm.pred)
  # Calculate current accuracy, and add to the list of accuracies
  accuracies <- c(accuracies, sum(diag(confusion)) / sum(confusion)) 
}

print(date())
# Calculate the mean accuracy from the 10 tests
mean_accuracy <- mean(accuracies)
mean_accuracy
```

```{r}
accuracies <- NULL

for (set in cv.sets) {
  # The current row numbers (`set`) corresponds to the test data, so
  # for training, we use `-set` to use all samples but the test samples.
  svm.model <- svm(cancer.train, classes.reduced.train, kernel = "linear")
  svm.pred <- predict(svm.model, cancer.test)
  confusion <- table(classes.reduced.test, svm.pred)
  # Calculate current accuracy, and add to the list of accuracies
  accuracies <- c(accuracies, sum(diag(confusion)) / sum(confusion)) 
}

mean_accuracy <- mean(accuracies)
mean_accuracy
```

```{r}
#making correlatoin plot for features

test <- t(reduced.genes.data)

corr_mat=cor(test,method="s") #create Spearman correlation matrix

library("corrplot")
corrplot(corr_mat, method = "color",
     diag = TRUE, order = "hclust",
     hclust.method = c("ward.D2"),
     tl.srt = 45,
     tl.pos='n'
     #addCoef.col = "black",
     #tl.col = "black"
     )  


```



```{r}
library(pheatmap)
macolor = colorRampPalette(c("navyblue", "white", "red"))(100)
test2 <- reduced.genes.data

corr_mat=cor(test,method="s") #create Spearman correlation matrix

test2.map <- pheatmap(corr_mat, color = rev(macolor), clustering_method = "ward.D2", fontsize_row = 0.8, fontsize_col = 0.8)

```

